﻿namespace Services.Abstractions.Sales
{
    public interface IServiceManager
    {
        ISaleService SaleService { get; }
    }
}
