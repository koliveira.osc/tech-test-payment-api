﻿using Contracts.Sales;
using Enums;
using System.Threading.Tasks;

namespace Services.Abstractions.Sales
{
    public interface ISaleService
    {
        Task<OrderDTO> GetByIdAsync(long id);
        Task InsertAsync(SaleDTO saleDTO);
        Task UpdateAsync(long id, OrderStatus Status);
    }
}
