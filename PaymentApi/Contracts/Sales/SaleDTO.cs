﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts.Sales
{
    public class SaleDTO
    {
        [Required(ErrorMessage = "CPF do vendedor é obrigatório")]
        public string CPF { get; set; }

        [Display(Description = "Nome do Vendedor")]
        [Required(ErrorMessage = "Nome do vendedor e obrigatório")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email do vendedor é obrigatório")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email inválido")]
        public string Email { get; set; }

        [Display(Description = "Telefone")]
        [Required(ErrorMessage = "Telefone do vendedor é obrigatório")]
        public string PhoneNumber { get; set; }

        public List<SaleItemDTO> Items { get; set; }
    }
}