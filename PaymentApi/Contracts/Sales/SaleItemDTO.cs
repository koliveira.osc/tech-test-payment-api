﻿using System.ComponentModel.DataAnnotations;

namespace Contracts.Sales
{
    public class SaleItemDTO
    {   
        [Display(Description = "Descição")]
        [Required(ErrorMessage = "Descrição do item é obrigatória")]
        public string Description { get; set; }

        [Display(Description = "Quantidade")]
        [Range(1, int.MaxValue, ErrorMessage = "Quantidade de intes é obrigatória")]
        public int Quantity { get; set; }
    }
}
