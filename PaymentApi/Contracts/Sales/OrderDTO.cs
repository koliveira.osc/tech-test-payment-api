﻿using Enums;
using System;
using System.Collections.Generic;

namespace Contracts.Sales
{
    public class OrderDTO
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public Guid Order { get; set; }
        public OrderStatus OrderStatus { get; set; }

        public SalesmanDTO Salesman { get; set; }
        public List<SaleItemDTO> Items { get; set; }
    }
}
