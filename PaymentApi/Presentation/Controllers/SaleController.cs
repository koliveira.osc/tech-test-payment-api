﻿using Contracts.Sales;
using Enums;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.Sales;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;

        public SaleController(IServiceManager serviceManager) => _serviceManager = serviceManager;

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOrderById(long id)
        {
            if (!(id > 0)) return BadRequest("Id da venda é obrigatório");

            OrderDTO order = await _serviceManager.SaleService.GetByIdAsync(id);
            return Ok(order);
        }

        [HttpPost]
        public async Task<IActionResult> RegisterSale(SaleDTO sale)
        {
            if (!ModelState.IsValid) return BadRequest();

            await _serviceManager.SaleService.InsertAsync(sale);
            return Ok();
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> ChangeStatusOrder(long id, OrderStatus newStatus)
        {
            if (!(id > 0)) return BadRequest("Id da venda é obrigatório");

            await _serviceManager.SaleService.UpdateAsync(id, newStatus);
            return Ok();
        }
    }
}
