﻿using Domain.Repositories;
using Services.Abstractions.Sales;
using System;

namespace Services
{
    public sealed class ServiceManager : IServiceManager
    {
        private readonly Lazy<ISaleService> _lazySaleService;

        public ServiceManager(IRepositoryManager repositoryManager)
        {
            _lazySaleService = new Lazy<ISaleService>(() => new SalesService(repositoryManager));
        }

        public ISaleService SaleService => _lazySaleService.Value;

    }
}
