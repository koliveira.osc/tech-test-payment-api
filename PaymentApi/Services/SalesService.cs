﻿using Contracts.Sales;
using Domain.Entities;
using Domain.Exceptions.Sales;
using Domain.Repositories;
using Enums;
using Mapster;
using Services.Abstractions.Sales;
using Services.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Services
{
    internal sealed class SalesService : ISaleService
    {
        private readonly IRepositoryManager _repositoryManager;

        public SalesService(IRepositoryManager repositoryManager) => _repositoryManager = repositoryManager;

        public async Task<OrderDTO> GetByIdAsync(long id)
        {
            try
            {
                Sale order = _repositoryManager.SaleRepository.GetById(id, "Salesman", "SalesItems");
                if (order == null) throw new OrderNotFoundException(id);

                OrderDTO orderDto = order.Adapt<OrderDTO>();
                orderDto.Salesman = order.Salesman.Adapt<SalesmanDTO>();
                orderDto.Items = order.SalesItems.Adapt<List<SaleItemDTO>>();

                return orderDto;
            }
            catch
            {
                throw;
            }
        }

        public async Task InsertAsync(SaleDTO saleDTO)
        {
            try
            {   
                // CHECK CPF
                if (!ValidationsHelper.IsValidCpf(saleDTO.CPF)) throw new ValidationException("O Número de CPF do vendedor é inválido");

                // CHECK items
                if (!saleDTO.Items.Any()) throw new ValidationException("Venda sem Items. É necessário informar ao menos 1 item.");

                Salesman seller = saleDTO.Adapt<Salesman>();
                Sale sale = new Sale()
                {
                    SalesItems = saleDTO.Items.Adapt<List<SaleItem>>(),
                    Salesman = CheckSeller(seller)
                };

                _repositoryManager.SaleRepository.Insert(sale);
                _repositoryManager.UnitOfWork.Commit();
            }
            catch(System.Exception ex)
            {
                _repositoryManager.UnitOfWork.Rollback();
                throw;
            }
        }

        public async Task UpdateAsync(long id, OrderStatus newStatus)
        {
            try
            {
                Sale order = _repositoryManager.SaleRepository.GetById(id);
                if (order == null) throw new OrderNotFoundException(id);

                CheckOrderStatus(order.OrderStatus, newStatus);

                order.OrderStatus = newStatus;
                _repositoryManager.SaleRepository.Update(order);
                _repositoryManager.UnitOfWork.Commit();
            }
            catch
            {
                _repositoryManager.UnitOfWork.Rollback();
                throw;
            }
        }
        
        private Salesman CheckSeller(Salesman salesman)
        {   
            Salesman seller = _repositoryManager.SalesmanRepository.GetByCPF(salesman.CPF);
            return seller ?? salesman;
        }

        private void CheckOrderStatus(OrderStatus currentOrderStatus, OrderStatus newStatus)
        {
            string errorMsg = $"Não é possível atualizar a venda para o status [{newStatus}]";
            
            switch (currentOrderStatus)
            {
                case OrderStatus.PendingPayment:
                    if (newStatus != OrderStatus.PaymentAccept && newStatus != OrderStatus.CanceledSale)
                        throw new NotAllowedException(errorMsg);
                    break;
                case OrderStatus.PaymentAccept:
                    if (newStatus != OrderStatus.ShippedCarrier && newStatus != OrderStatus.CanceledSale)
                        throw new NotAllowedException(errorMsg);
                    break;
                case OrderStatus.ShippedCarrier:
                    if (newStatus != OrderStatus.OrderDelivered)
                        throw new NotAllowedException(errorMsg);
                    break;
                case OrderStatus.OrderDelivered:
                case OrderStatus.CanceledSale:
                default:
                    throw new NotAllowedException(errorMsg);
                    break;
            }
        }
    }
}
