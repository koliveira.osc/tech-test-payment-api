﻿using System.ComponentModel;

namespace Enums
{
    public enum OrderStatus : sbyte
    {
        [Description("Aguardando pagamento")]
        PendingPayment = 1,

        [Description("Pagamento aprovado")]
        PaymentAccept = 2,

        [Description("Enviado para transportadora")]
        ShippedCarrier = 3,

        [Description("Entregue")]
        OrderDelivered = 4,

        [Description("Cancelada")]
        CanceledSale = 5
    }
}
