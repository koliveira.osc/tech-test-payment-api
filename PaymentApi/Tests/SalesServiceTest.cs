﻿using Contracts.Sales;
using Domain.Exceptions.Sales;
using Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalesApi;
using Services.Abstractions.Sales;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass]
    public class SalesServiceTest
    {
        // Utiliza a injeção de depedência do projeto de API
        readonly IServiceProvider _services =
            Program.CreateHostBuilder(new string[] { }).Build().Services;

        private IServiceManager _serviceManager;


        public SalesServiceTest()
        {
            _serviceManager = _services.GetRequiredService<IServiceManager>();
        }

        [TestMethod]
        public async Task Sale_PerfectScenery()
        {
            SaleDTO dto = new SaleDTO()
            {
                // CPF gerado através do site https://www.4devs.com.br/gerador_de_cpf
                CPF = "60688039022",
                Name = "Kayo Oliveira",
                Email = "koliveira.osc@gmail.com",
                PhoneNumber = "999999-9999",
                Items = new List<SaleItemDTO>()
                {
                    new SaleItemDTO() { Description = "Item A", Quantity = 10 },
                    new SaleItemDTO() { Description = "Item B", Quantity = 5 },
                    new SaleItemDTO() { Description = "Item C", Quantity = 20 }
                }
            };

            await _serviceManager.SaleService.InsertAsync(dto);
        }

        [TestMethod]
        public async Task Sale_PerfectScenery_UpdateStatus_PendingPaymentToPaymentAccept()
        {
            //Aguardando pagamento` Para: `Pagamento Aprovado`
            SaleDTO dto = new SaleDTO()
            {
                // CPF gerado através do site https://www.4devs.com.br/gerador_de_cpf
                CPF = "60688039022",
                Name = "Kayo Oliveira",
                Email = "koliveira.osc@gmail.com",
                PhoneNumber = "999999-9999",
                Items = new List<SaleItemDTO>()
                {
                    new SaleItemDTO() { Description = "Item A", Quantity = 10 },
                    new SaleItemDTO() { Description = "Item B", Quantity = 5 },
                    new SaleItemDTO() { Description = "Item C", Quantity = 20 }
                }
            };

            await _serviceManager.SaleService.InsertAsync(dto);

            await Task.Delay(1000);
            await _serviceManager.SaleService.UpdateAsync(1, OrderStatus.CanceledSale);
        }

        [TestMethod]
        [ExpectedException(typeof(NotAllowedException))]
        public async Task Sale_FailureScenery_UpdateStatus_PendingPaymentToPaymentAccept()
        {
            //Aguardando pagamento` Para: `Pagamento Aprovado`
            SaleDTO dto = new SaleDTO()
            {
                // CPF gerado através do site https://www.4devs.com.br/gerador_de_cpf
                CPF = "60688039022",
                Name = "Kayo Oliveira",
                Email = "koliveira.osc@gmail.com",
                PhoneNumber = "999999-9999",
                Items = new List<SaleItemDTO>()
                {
                    new SaleItemDTO() { Description = "Item A", Quantity = 10 },
                    new SaleItemDTO() { Description = "Item B", Quantity = 5 },
                    new SaleItemDTO() { Description = "Item C", Quantity = 20 }
                }
            };

            await _serviceManager.SaleService.InsertAsync(dto);
            await _serviceManager.SaleService.UpdateAsync(1, OrderStatus.PaymentAccept);

            // cria um pequeno intervalo entre os updates
            await Task.Delay(1000);
            await _serviceManager.SaleService.UpdateAsync(1, OrderStatus.ShippedCarrier);

            await Task.Delay(1000);
            await _serviceManager.SaleService.UpdateAsync(1, OrderStatus.PaymentAccept);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public async Task Sale_Invalid_CPF()
        {
            SaleDTO dto = new SaleDTO()
            {   
                // NO CPF
                Name = "Kayo Oliveira",
                Email = "koliveira.osc@gmail.com",
                PhoneNumber = "999999-9999",
                Items = new List<SaleItemDTO>()
                {
                    new SaleItemDTO() { Description = "Item A", Quantity = 10 },
                    new SaleItemDTO() { Description = "Item B", Quantity = 5 },
                }
            };

            await _serviceManager.SaleService.InsertAsync(dto);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public async Task Sale_WihtoutItems()
        {
            SaleDTO dto = new SaleDTO()
            {
                Name = "Kayo Oliveira",
                Email = "koliveira.osc@gmail.com",
                PhoneNumber = "999999-9999",
                Items = new List<SaleItemDTO>()
                {
                    // no items
                }
            };

            await _serviceManager.SaleService.InsertAsync(dto);
        }
    }
}
