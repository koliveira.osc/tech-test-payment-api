﻿using Domain.Repositories;
using Domain.Repositories.Base;
using Domain.Repositories.Sales;
using Persistence.Repositories.Sales;
using System;

namespace Persistence.Repositories
{
    public sealed class RepositoryManager : IRepositoryManager
    {
        private readonly Lazy<ISaleRepository> _lazySaleRepository;
        private readonly Lazy<ISaleItemRepository> _lazySaleItemRepository;
        private readonly Lazy<ISalesmanRepository> _lazySalesmanRepository;
        private readonly Lazy<IUnitOfWork> _lazyUnitOfWork;

        public RepositoryManager(PaymentDbContext dbContext)
        {
            _lazySaleRepository = new Lazy<ISaleRepository>(() => new SaleRepository(dbContext));
            _lazySaleItemRepository = new Lazy<ISaleItemRepository>(() => new SaleItemRepository(dbContext));
            _lazySalesmanRepository = new Lazy<ISalesmanRepository>(() => new SalesmanRepository(dbContext));
            _lazyUnitOfWork = new Lazy<IUnitOfWork>(() => new UnitOfWork(dbContext));
        }

        public ISaleRepository SaleRepository => _lazySaleRepository.Value;
        public ISaleItemRepository SaleItemRepository => _lazySaleItemRepository.Value;
        public ISalesmanRepository SalesmanRepository => _lazySalesmanRepository.Value;
        public IUnitOfWork UnitOfWork => _lazyUnitOfWork.Value;
    }
}
