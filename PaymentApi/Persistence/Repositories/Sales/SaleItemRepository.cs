﻿using Domain.Entities;
using Domain.Repositories.Sales;

namespace Persistence.Repositories.Sales
{
    internal sealed class SaleItemRepository : RepositoryBase<SaleItem>, ISaleItemRepository
    {
        public SaleItemRepository(PaymentDbContext pottencialTesteDbContext)
            : base(pottencialTesteDbContext)
        {
        }
    }
}
