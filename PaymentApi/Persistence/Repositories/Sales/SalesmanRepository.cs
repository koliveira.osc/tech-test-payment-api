﻿using Domain.Entities;
using Domain.Repositories.Sales;
using System.Linq;

namespace Persistence.Repositories.Sales
{
    internal sealed class SalesmanRepository : RepositoryBase<Salesman>, ISalesmanRepository
    {
        public SalesmanRepository(PaymentDbContext paymentDbContext) 
            : base(paymentDbContext)
        {
        }

        public Salesman GetByCPF(string cpf)
        {
            return Get(w => w.CPF == cpf).FirstOrDefault();
        }
    }
}
