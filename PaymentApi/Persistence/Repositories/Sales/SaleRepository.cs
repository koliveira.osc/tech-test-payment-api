﻿using Domain.Entities;
using Domain.Repositories.Sales;

namespace Persistence.Repositories.Sales
{
    internal sealed class SaleRepository : RepositoryBase<Sale>, ISaleRepository
    {
        public SaleRepository(PaymentDbContext pottencialTesteDbContext)
            : base(pottencialTesteDbContext)
        {
        }
    }
}
