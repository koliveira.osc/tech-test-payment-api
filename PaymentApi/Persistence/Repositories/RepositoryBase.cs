﻿using Domain.Entities;
using Domain.Repositories.Base;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Persistence.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> 
        where TEntity : IdentityEntity
    {
        private readonly PaymentDbContext dbContext;

        public RepositoryBase(PaymentDbContext paymentDbContext)
        {
            this.dbContext = paymentDbContext;
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return dbContext.GetDbSetWithIncludes<TEntity>(includes).Where(criteria);
        }

        public TEntity GetById(long id, params string[] includes)
        {
            return dbContext.GetDbSetWithIncludes<TEntity>(includes).FirstOrDefault(e => e.Id == id);
        }

        public void Update(TEntity entity)
        {   
            dbContext.Set<TEntity>().Update(entity);
        }

        public void Delete(TEntity entity)
        {
            dbContext.Set<TEntity>().Remove(entity);
        }

        public void Insert(TEntity entity)
        {
            dbContext.Set<TEntity>().Add(entity);
        }
    }
}
