﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.EntityMap
{
    public class SaleItemRepositoryMap : IEntityTypeConfiguration<SaleItem>
    {
        public void Configure(EntityTypeBuilder<SaleItem> builder)
        {
            builder.ToTable("VendaItens");

            builder.HasKey(k => k.Id);

            builder.Property(p => p.Description)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("Descricao");

            builder.Property(p => p.Quantity)
                .IsRequired()
                .HasColumnName("Quantidade");

            builder.HasOne<Sale>()
                .WithMany(map => map.SalesItems)
                .HasForeignKey(map => map.SaleId);
        }
    }
}
