﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.EntityMap
{
    public class SalesRepositoryMap : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.ToTable("Vendas");

            builder.HasKey(k => k.Id);

            builder.Property(p => p.SalesmanID)
                .IsRequired()
                .HasColumnName("VendedorId");

            builder.Property(P => P.Order)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnName("NumeroPedido");

            builder.Property(p => p.OrderStatus)
                .IsRequired()
                .HasColumnName("Status");

            builder.Property(p => p.Date)
                .IsRequired()
                .HasColumnName("DataVenda");

            builder.HasOne<Salesman>(m => m.Salesman)
                .WithMany(g => g.Sales)
                .HasForeignKey(m => m.SalesmanID);
        }
    }
}
