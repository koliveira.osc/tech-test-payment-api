﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.EntityMap
{
    internal sealed class SalesmanRepositoryMap : IEntityTypeConfiguration<Salesman>
    {
        public void Configure(EntityTypeBuilder<Salesman> builder)
        {
            builder.ToTable("Vendedor");

            builder.HasIndex(i => i.CPF, "uidx_cpf")
                .IsUnique();

            builder.HasKey(k => k.Id);

            builder.Property(p => p.Id)
                .ValueGeneratedOnAdd();

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(120)
                .HasColumnName("Nome");

            builder.Property(p => p.CPF)
                .IsRequired()
                .HasMaxLength(11);

            builder.Property(p => p.Email)
                .IsRequired()
                .HasMaxLength(80);

            builder.Property(p => p.PhoneNumber)
                .IsRequired()
                .HasMaxLength(15)
                .HasColumnName("Telefone");            
        }
    }
}
