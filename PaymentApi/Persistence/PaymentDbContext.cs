﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.EntityMap;
using System.Linq;

namespace Persistence
{
    public class PaymentDbContext : DbContext
    {
        DbSet<Sale> Sales { get; set; }
        DbSet<SaleItem> SaleItems { get; set; }
        DbSet<Salesman> Salesmen { get; set; }

        public PaymentDbContext(DbContextOptions<PaymentDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) =>
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PaymentDbContext).Assembly);

        public IQueryable<TEntity> GetDbSetWithIncludes<TEntity>(params string[] includes) 
            where TEntity : IdentityEntity
        {
            IQueryable<TEntity> dbSet = Set<TEntity>();

            if (includes?.Length > 0)
                foreach (var include in includes)
                    dbSet = dbSet.Include(include);

            return dbSet;
        }
    }
}
