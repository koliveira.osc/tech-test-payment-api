﻿using Domain.Repositories.Base;
using Domain.Repositories.Sales;

namespace Domain.Repositories
{
    public interface IRepositoryManager
    {
        ISaleRepository SaleRepository { get; }
        ISaleItemRepository SaleItemRepository { get; }
        ISalesmanRepository SalesmanRepository { get; }
        IUnitOfWork UnitOfWork { get; }
    }
}
