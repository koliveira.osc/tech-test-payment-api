﻿using Domain.Entities;
using Domain.Repositories.Base;

namespace Domain.Repositories.Sales
{
    public interface ISaleItemRepository : IRepositoryBase<SaleItem>
    {
    }
}
