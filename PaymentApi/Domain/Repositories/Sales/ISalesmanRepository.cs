﻿using Domain.Entities;
using Domain.Repositories.Base;

namespace Domain.Repositories.Sales
{
    public interface ISalesmanRepository : IRepositoryBase<Salesman>
    {
        Salesman GetByCPF(string cpf);
    }
}
