﻿using Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Repositories.Base
{
    public interface IRepositoryBase<TEntity> 
        where TEntity : IdentityEntity
    {
        TEntity GetById(long id, params string[] includes);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
