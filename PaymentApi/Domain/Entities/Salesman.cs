﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Salesman : IdentityEntity 
    {
        public string CPF { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }
    }
}
