﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class SaleItem : IdentityEntity
    {
        public long SaleId { get; set; }
        public string Description { get; set; }
        public ushort Quantity { get; set; }
    }
}
