﻿using Enums;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Sale : IdentityEntity
    {
        public long SalesmanID { get; set; }
        public DateTime Date { get; set; }
        public Guid Order { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public Salesman Salesman { get; set; }
        public ICollection<SaleItem> SalesItems { get; set; }

        public Sale()
        {
            Order = Guid.NewGuid();
            OrderStatus = OrderStatus.PendingPayment;
            Date = DateTime.Now;
        }
    }
}
