﻿namespace Domain.Exceptions.Sales
{
    public sealed class OrderNotFoundException : NotFoundException
    {
        public OrderNotFoundException(long id)
            : base($"Não foi encontrado nenhum pedido com o código: {id}.")
        {
        }
    }
}
