﻿using System;

namespace Domain.Exceptions.Sales
{
    public sealed class NotAllowedException : Exception
    {
        public NotAllowedException(string msg)
            : base(msg)
        {
        }
    }
}
