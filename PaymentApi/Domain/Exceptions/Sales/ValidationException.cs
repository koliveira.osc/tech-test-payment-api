﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.Sales
{
    public sealed class ValidationException : BadRequestException
    {
        public ValidationException(string message) :
            base(message)
        {
        }
    }
}
